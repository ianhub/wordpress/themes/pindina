<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Pindina theme search form file.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
?>
<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="search-form">
<input type="text" name="s" id="s" class="search-input" placeholder="<?php esc_attr_e( 'Search...', 'pindina' ); ?>">
<button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
</form>

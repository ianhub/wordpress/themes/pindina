<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Pindina theme footer file.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
?>
		</div><!--/.container-->
	</div><!--/.wrapper-->
<?php wp_footer(); ?>
</body>
</html>

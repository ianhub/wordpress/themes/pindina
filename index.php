<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Pindina index file.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */

// Let's see what template part to load first.
$template_slug = 'post/loop';
$template_name = null;
$use_sidebar   = true;

// In case of a static front page.
if ( is_front_page() && ! is_home() ) {
	$template_slug = 'page/content';
}
// In case of blog page.
elseif ( ( is_home() && ! is_front_page() ) OR is_category() ) {
	$template_slug = 'post/loop';
}
// In case of a single post content.
elseif ( is_single() ) {
	$template_slug = 'post/content';
	$template_name = get_post_type();
}
// In case of a page.
elseif ( is_page() ) {
	$template_slug = 'page/content';
}

// Load theme header file.
get_header();

// Load the selected template.
get_template_part( 'template-parts/' . $template_slug, $template_name );

// Should we load the sidebar?
if ( $use_sidebar === true ) {
	get_sidebar();
}

// Load theme footer file.
get_footer();

<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Pindina theme header file.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    // We let WordPress handle the rest.
    wp_head();
    ?>
</head>
<body <?php body_class(); ?>>
	<header class="header" id="header" role="banner">
		<div class="container">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><?php bloginfo( 'name' ); ?></a>
			<?php get_template_part( 'template-parts/navigation/menu', 'main' ); ?>

			<div class="header-right">
				<?php
				// Load our social links menu.
				get_template_part( 'template-parts/navigation/menu', 'social' );

				// Load our search form template.
				get_search_form();
				?>
			</div><!--/.float-right-->
		</div><!--/.container-->
	</header><!--/.header-->
	<div class="wrapper" id="wrapper">
		<div class="container">

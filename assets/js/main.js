/*!
 * main.js file.
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Assets/JS
 * @author 	Kader Bouyakoub (bkader@mail.com)
 * @link 	https://github.com/bkader
 */
(function ($) {
	// If jQuery if not found, nothing to do.
	if ( $ === undefined ) {
		return;
	}

	// Avoid conflict with other libraries.
	jQuery.noConflict();
	"use strict";

	// Things to do when DOM is ready.
	jQuery(document).ready(function () {

		// Hold the main mobile menu.
		var mobile_menu = jQuery(".main-menu");

		// Mobile menu.
		jQuery(".menu-toggle").on("click", function (e) {
			e.preventDefault();
			if (mobile_menu.hasClass("open")) {
				mobile_menu.slideUp("fast", function () {
					jQuery(this).removeClass("open");
				});
			} else {
				mobile_menu.slideDown("fast", function () {
					jQuery(this).addClass("open");
				});
			}
		});

		// Things to do if the user clicks on the document.
		jQuery(document).on("click", function (e) {

			// We make sure to hide the mobile menu.
			if (mobile_menu.hasClass("open")) {
				mobile_menu.slideUp("fast", function () {
					jQuery(this).removeClass("open");
				});
			}
		});
	});

})(jQuery);

<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Pindina functions.php file.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Functions
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
// ------------------------------------------------------------------------
if ( ! class_exists( 'Pindina', false ) ):
// ------------------------------------------------------------------------

// Define needed path and URL.
defined('PINDINA') OR define('PINDINA', get_template_directory());
defined('PINDINA_URL') OR define('PINDINA_URL', get_template_directory_uri());

/**
 * Pindina Class
 *
 * The reason we are using a class here is to avoid any possible
 * conflict with any child themes or plugins.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Category
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
class Pindina {
	/**
	 * __construct
	 *
	 * Class constructor to initialize theme preferences.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	none
	 * @return 	void
	 */
	public function __construct() {

		// Things to be done upon theme setup.
		add_action( 'after_setup_theme', array( $this, 'setup' ) );

		// Things to be done only on the front page.
		if ( ! is_admin() ) {
			// Make sure to enqueue our styles and scripts.
			add_action( 'wp_enqueue_scripts', array( $this, 'assets' ) );

			// Make sure to remove WordPress generator tag.
			add_filter( 'the_generator', array( $this, 'the_generator' ) );

			// Better WordPress excerpt for this theme.
			add_filter( 'get_the_excerpt', array( $this, 'get_the_excerpt' ) );

			// Prevent direct access to attachments pages.
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );

			// Add post views to post object.
			add_action( 'the_post', array( $this, 'the_post' ) );
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * setup method
	 *
	 * This method handles things to be done upon theme's installation.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	none
	 * @return 	void
	 */
	public function setup() {
		// We make sure to load theme's text domain.
		load_theme_textdomain( 'pindina', PINDINA.'/languages' );

		// Add support for title tag.
		add_theme_support('title-tag');

		// Add RSS feed links to <head> for posts and comments.
		add_theme_support('automatic-feed-link');

		// Add support for thumbnails.
		add_theme_support('post-thumbnails', array('post', 'page'));

		// Add image sizes.
		add_image_size( 'loop-thumb', 300, 170, true );

		/**
		 * Switch default core markup for search form,
		 * comment form, and comments to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		// Add support for featured posts.
		add_theme_support( 'featured-post', array(
			'max_posts'  => 3,					// Limit featured posts to 3.
			'post_types' => array( 'post' ),	// Only posts can be featured.
		) );

		// Register theme menus.
		register_nav_menus(array(
			'footer' => esc_html__('Footer Menu', 'pindina'),	// Footer Menu.
			'main'   => esc_html__('Main Menu', 'pindina'),		// Homepage menus.
			'social' => esc_html__('Social Links', 'pindina'),	// Social links menu.
		));

		// Register theme's widgets.
		register_sidebar( array(
			'id'            => 'sidebar',
			'name'          => esc_html__( 'Sidebar', 'pindina' ),
			'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
			'after_widget'  => '</div>',
		) );
	}

	// ------------------------------------------------------------------------

	/**
	 * assets method
	 *
	 * This method enqueues all needed styles and scripts on the front end.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	none
	 * @return 	void
	 */
	public function assets() {
		// Enqueue FontAwesome.
		wp_enqueue_style( 'fontawesome', PINDINA_URL.'/assets/css/font-awesome.min.css' );

		// We make sure to enqueue our main StyleSheet.
		wp_enqueue_style( 'style', PINDINA_URL.'/style.css', array( 'fontawesome') );

		// Register our main script file and add needed parameters.
		wp_register_script( 'main', PINDINA_URL.'/assets/js/main.js', array( 'jquery' ) );
		wp_localize_script( 'main', 'Pindina', array(
			'homeURL' => home_url(),
			'ajaxURL' => admin_url('admin-ajax.php'),
		) );
		wp_enqueue_script( 'main' );

		// We make sure to move all JS files to footer.
		remove_action('wp_head', 'wp_print_scripts');
		remove_action('wp_head', 'wp_print_head_scripts', 9);
		remove_action('wp_head', 'wp_enqueue_scripts', 1);
		add_action('wp_footer', 'wp_print_scripts', 5);
		add_action('wp_footer', 'wp_enqueue_scripts', 5);
		add_action('wp_footer', 'wp_print_head_scripts', 5);

		// Add our signature to document source code.
		add_action( 'wp_head', array( $this, 'signature' ) );
	}

	// ------------------------------------------------------------------------
	// Posts methods.
	// ------------------------------------------------------------------------

	/**
	 * no_attachment_access
	 *
	 * This method simply prevents direct access to attachments pages.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	void
	 * @return 	void
	 */
	public function no_attachment_access() {
		// Is the user on the attachment page?
		if ( is_attachment() ) {

			// Get the attachment parent post.
			global $post;
			if ( $post && $post->post_parent ) {
				wp_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
				exit;
			}
			// No parent? Redirect to homepage.
			else {
				wp_redirect( esc_url( home_url( '/' ) ), 301 );
				exit;
			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * get_post_views
	 *
	 * This method retrieve a single post views.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	object
	 * @return 	object
	 */
	public function get_post_views( $post ) {

		// Make sure to use it only of posts.
		if ( 'post' == $post->post_type ) {
			$key = 'post_views_count';
			$count = get_post_meta( $post->ID, $key, true );

			// Not found? Create it.
			if ( $count == '' ) {
				$count = 0;
				delete_post_meta( $post->ID, $key );
				add_post_meta( $post->ID, $key, $count );
			}

			// Add the count to post object.
			$post->views = $count;
		}
		return $post;
	}

	// ------------------------------------------------------------------------

	/**
	 * set_post_views
	 *
	 * This method sets/increments a single post views counter.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	int  	$id 	The post's ID.
	 * @return 	void
	 */
	public function set_post_views( $id ) {
		$key = 'post_views_count';
		$count = get_post_meta( $id, $key, true );

		// Not found? Set it.
		if ( $count == '' ) {
			$count = 0;
			delete_post_meta( $id, $key );
			add_post_meta( $id, $key, '0' );
		}
		// Otherwise, increment it.
		else {
			$count++;
			update_post_meta( $id, $key, $count );
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * posts_views_table_column
	 *
	 * This method adds the views counter to posts table and removes
	 * unnecessary columns, suck us ones added by Yoast... etc.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	array
	 * @return 	array
	 */
	public function posts_views_table_column( $columns ) {
		// Add our views column.
		$columns['post_views_count'] = esc_html__( 'Views', 'pindina' );

		// Remove unnecessary columns.
		unset(
			$columns['comments'],
			$columns['seotitle'],
			$columns['seodesc'],
			$columns['seokeywords']
		);

		// Return the new columns array.
		return $columns;
	}

	// ------------------------------------------------------------------------

	/**
	 * posts_views_table_content
	 *
	 * This method fills up posts views counter for each posts;
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	string 	$column_name 	The column name.
	 * @param 	int 	$post_id 		The post's ID.
	 * @return 	void
	 */
	public function posts_views_table_content( $column_name, $post_id ) {
		// Views column?
		if ( $column_name == 'post_views_count' ) {
			echo intval( get_post_meta( $post_id, 'post_views_count', true ) );
		}
	}

	// ------------------------------------------------------------------------
	// Utilities.
	// ------------------------------------------------------------------------

	/**
	 * signature
	 *
	 * This method simply adds a little HTML comment on your site's source
	 * code in order to keep a reference of the work only.
	 * Although this theme is free, we are proud that you have chosen it and
	 * are using it. We add this comment just in case we are asked of works
	 * we have done and tell the client about it and ask him to check the
	 * source code to see my name there, a little trick to say I am not lying.
	 *
	 * Feel free to remove this method if you want, it's you right after all.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	none
	 * @return 	void
	 */
	public function signature() {
		echo <<<EOT
<!--noptimize--><!-- This site's author has willingly accepted to keep this comment on his/her code. For that, my sincere thanks and appreciation. This theme, Pindina, was created with so much love by Kader Bouyakoub <bkader[at]mail.com> or Ianhub™ (ianhub.net). More about the author: http://bit.ly/2pfKFyN
--><!--/noptimize-->
EOT;
	}

	// ------------------------------------------------------------------------

	/**
	 * the_generator
	 *
	 * This method simply removes the WordPress generator meta tag.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	none
	 * @return 	string
	 */
	public function the_generator() {
		return '';
	}

	// ------------------------------------------------------------------------

	/**
	 * get_the_excerpt
	 *
	 * Because we are using a limited number characters for
	 * our post excerpt, we make sure to limit it.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	string 	$excerpt 	The original excerpt.
	 * @return 	string 	The excerpt after being reduced.
	 */
	public function get_the_excerpt( $excerpt ) {
		/**
		 * Here are things we are going to check before returning the excerpt:
		 * 1. We are on the blog section.
		 * 2. We are not on the attachment page.
		 */
		if ( 'post' !== get_post_type() ) {
			return $excerpt;
		}

		// We make sure to split, then add the read more link.
		if ( ! is_attachment() ) {
			$sub = '';
			$len = 0;

			// We make sure to strip everything.
			$excerpt = strip_tags( strip_shortcodes( $excerpt ) );

			// We explode the excerpt.
			$exp = explode( ' ', $excerpt );

			// Let's format it now.
			foreach ( $exp as $word ) {
				$part = ( ( $sub != '' ) ? ' ' : '' ) . $word;
				$sub .= $part;
				$len += strlen( $part );

				if ( strlen( $word ) > 3 && strlen( $sub ) >= 180 ) {
					break;
				}
			}

			// Put everything back together.
			$excerpt = $sub . ( ( $len < strlen( $str ) ) ? ' ..' : '');

			// We finally add the read more link.
			$read_more = ' <a class="read-more" href="%1$s">%2$s</a>';
			$excerpt .= sprintf(
				$read_more,
				get_the_permalink(),
				esc_html__( 'Read More', 'pindina' )
			);
		}

		// Return the excerpt.
		return $excerpt;
	}

	// ------------------------------------------------------------------------

	/**
	 * template_redirect
	 *
	 * This method simply prevents access to attachments files.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	none
	 * @return 	void
	 */
	public function template_redirect() {
		// Is the user on the attachment page?
		if ( is_attachment() ) {

			// Get the attachment parent post.
			global $post;
			if ( $post && $post->post_parent ) {
				wp_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
				exit;
			}
			// No parent? Redirect to homepage.
			else {
				wp_redirect( esc_url( home_url( '/' ) ), 301 );
				exit;
			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * the_post
	 *
	 * This method simply adds post's views counter to its object.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	object 	$post 	The post's object.
	 * @return 	object
	 */
	public function the_post( $post ) {
		return $this->get_post_views( $post );
	}

}

// We no initialize class.
$pindina = new Pindina();

// ------------------------------------------------------------------------

if ( ! function_exists( 'pindina_set_post_views' ) ) {
	/**
	 * pindina_set_post_views
	 *
	 * This function simply uses the Pindina's method 'set_post_views'
	 * in order to set/increment single post views counter.
	 *
	 * @author 	Kader Bouyakoub
	 * @link 	https://github.com/bkader
	 * @since 	1.0.0
	 *
	 * @access 	public
	 * @param 	none
	 * @return 	void
	 */
	function pindina_set_post_views( $post_id ) {
		global $pindina;
		return $pindina->set_post_views( $post_id );
	}
}

// ------------------------------------------------------------------------
endif;
// ------------------------------------------------------------------------

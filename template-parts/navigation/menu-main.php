<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Main theme's menu.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
// Is the main menu provided? Display it.
if ( has_nav_menu( 'main' ) ) {
	wp_nav_menu( array(
		'theme_location' => 'main',
		'menu_class'     => 'main-menu',
		'container'      => false,
	) );
}

// We make sure to display mobile menu toggle.
echo '<a href="#" class="menu-toggle"><i class="fa fa-bars"></i></a>';

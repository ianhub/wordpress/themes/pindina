<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Theme's social links menu.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
// Do we have social menu links? Display theme.
if ( has_nav_menu( 'social' ) ) {
	wp_nav_menu( array(
		'theme_location' => 'social',
		'menu_class'     => 'social-menu',
		'container'      => false,
	) );
}

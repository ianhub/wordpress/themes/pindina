<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Pindina posts loop template.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
?><main class="main" id="main" role="main">
	<?php if ( have_posts() ):?>
	<div class="posts-loop">
	<?php while ( have_posts() ): the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
			<?php the_category(); // Display post categories. ?>
		<a href="<?php the_permalink(); ?>" class="thumbnail"><?php the_post_thumbnail( 'loop-thumb' ); ?></a>
		<ul class="sharer">
			<li><a href="javascript:void(0)" class="facebook"><i class="fa fa-facebook"></i></a></li>
			<li><a href="javascript:void(0)" class="twitter"><i class="fa fa-twitter"></i></a></li>
			<li><a href="javascript:void(0)" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
			<li><a href="javascript:void(0)" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
			<li><a href="javascript:void(0)" class="google"><i class="fa fa-google"></i></a></li>
		</ul>
		<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2><!--/.title-->
		<div class="author"><?php printf( esc_html__( 'Written by %s' ), get_the_author_posts_link()); edit_post_link( null, ' &#124; '); ?></div><!--/.author-->
		<div class="excerpt"><?php the_excerpt(); ?></div><!--/.excerpt-->
		<div class="meta">
			<span class="date"><i class="fa fa-clock-o"></i> <?php the_date(); ?></span>
			<span class="views"><i class="fa fa-eye"></i> <?php echo $post->views; ?> <span class="sr-only"><?php _e( 'Views', 'pindina' ); ?></span></span>
			<span class="rating">
				<span class="thumb-up"><i class="fa fa-thumbs-up"></i> 15</span>
				<span class="thumb-down"><i class="fa fa-thumbs-down"></i> 0</span>
			</span><!--/.rating-->
		</div><!--/.meta-->
		</article>
	<?php endwhile; ?>
	</div><!--/.posts-loop-->
<?php else: ?>
	<p><?php _e( 'There are currently no posts to display.', 'pindina' ); ?></p>
<?php endif; ?>
</main><!--/.content-->

<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Pindina single post template.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
?><main class="main" id="main" role="main">
<?php
	// The post is found?
	if ( have_posts() ):
		while ( have_posts() ):
			the_post();
			pindina_set_post_views( get_the_ID() );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	// If there are any widgets assigned here, display theme.
	if ( is_active_sidebar( 'above-content' ) ) {
		dynamic_sidebar( 'above-content' );
	}
	/**
	 * In case There are no widgets assign to this location and the
	 * wp-adposts plugin is used, try to display ads on this location.
	 */
	elseif ( function_exists( 'wp_adposts' ) ) {
		echo wp_adposts( 'above-content' );
	}
	?>
	<h1 class="title"><?php the_title(); ?></h1>
	<div class="meta">
		<?php
		// Display the author.
		printf( esc_html__( 'Written by %s' ), get_the_author_posts_link());

		// Display the date.
		the_date( null, ' &#124; ' );

		// Display post views counter.
		echo ' &#124; <i class="fa fa-eye"></i> ', $post->views;

		// Display the edit link for editors/authors/admins.
		edit_post_link( null, ' &#124; ' );

		?>
	</div><!--/.meta-->
	<?php
	// If the post has a thumbnail, display it.
	if ( has_post_thumbnail( get_the_ID() ) ) {
		the_post_thumbnail( 'single-thumb' );
	}
	?>
	<div class="content"><?php the_content(); ?></div><!--/.content-->
</article>
<?php endwhile; else: ?>
	<p>Post not found</p>
<?php endif; ?>
</main><!--/.main-->
